<?php
/**
 * Plugin Name:       WP-PostRatings Structured data fix
 * Description:       Fix Structured Data AgregateRating for wp-postratings
 * Version:           1.0.0
 * Author:            apollon.guru
 * Author URI:		  https://apollon.guru/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.

if ( ! defined( 'ABSPATH' ) ) {
    die;
}

// WordPress Structured Data AgregateRating Fix
add_filter('wp_postratings_google_structured_data', 'wp_pr_sd_fix');
function wp_pr_sd_fix($google_structured_data){
    global $post;

    if( get_query_var('amp') || !is_singular('post') ){
        return '';
    }

    if( is_singular() && is_main_query() ) {

        $ratings_max = (int) get_option( 'postratings_max' );
        $post_ratings_data = get_post_custom();
        $post_ratings_average = is_array( $post_ratings_data ) && array_key_exists( 'ratings_average', $post_ratings_data ) ? (float) $post_ratings_data['ratings_average'][0] : 0;
        $post_ratings_users = is_array( $post_ratings_data ) && array_key_exists( 'ratings_users', $post_ratings_data ) ? (int) $post_ratings_data['ratings_users'][0] : 0;

        $logo_img_id = get_option( '_ag_sd_logo' );
        $logo_img_src = wp_get_attachment_image_src( $logo_img_id, 'full' );
        $logo_img_src_url = is_array($logo_img_src) ? $logo_img_src[0] : null;

        if( has_post_thumbnail( $post->ID ) ){

            $image_url = get_the_post_thumbnail_url($post->ID);

        } elseif($logo_img_src_url) {

            $image_url = $logo_img_src_url;

        } else {

            $image_url = '{"@id": "#logo"}';

        }

        $url = get_permalink($post);

        $postratings_structured_data = array(
            '@context'      => 'http://schema.org',
            '@type'         => 'Article',
            'headline'      => $post->post_title,
            'datePublished' => get_the_date( 'c', $post ),
            'dateModified'  => get_post_modified_time('c', $post),
            'image'         => $image_url,
        );

        $yoast_description = get_post_meta($post->ID, '_yoast_wpseo_metadesc', true);

        $description = $yoast_description ? $yoast_description : $post->post_content;

        $description = wp_strip_all_tags(strip_shortcodes( $description ));

        if($description){

            $postratings_structured_data['description'] = $description;
            
        }

        $postratings_structured_data['publisher'] = array(
            '@type' => 'Organization',
            'name'  => get_bloginfo( 'name' ),
        );

        if($logo_img_src_url){

            $postratings_structured_data['publisher']['logo'] = array(
                '@type' => 'ImageObject',
                'url'   => $logo_img_src[0],
            );
            
        }

        $postratings_structured_data['mainEntityOfPage'] = $url;

        $author_user = get_userdata( $post->post_author );

        $postratings_structured_data['author'] = array(
            '@type' => 'Person',
            'name'  => $author_user->data->display_name
        );

        if( $post_ratings_average > 0 ) {

            $postratings_structured_data['aggregateRating'] = array(
                '@type'         => 'AggregateRating',
                'ratingValue'   => $post_ratings_average,
                'ratingCount'   => $post_ratings_users,
                'bestRating'    => $ratings_max,
                'worstRating'   => 1
            );
        }
        $google_structured_data = '<script type="application/ld+json" class="wp-postratings-structured-data">';
        $google_structured_data .= wp_json_encode($postratings_structured_data);
        $google_structured_data .= "</script>";
    }

    return $google_structured_data;

}

add_filter('wpseo_json_ld_output', 'wp_pr_sd_organization_logo_fix', 10, 2);
function wp_pr_sd_organization_logo_fix($data, $context){
    if('company' == $context){
        $logo_url = $data['logo'];
        $data['logo'] = array(
			"@id" => "#logo",
            "@type" => "ImageObject",
            "url" => $logo_url
        );
    }
    return $data;
}

add_filter( 'wp_postratings_schema_itemtype', '__return_false' );

add_action( 'wp_head', 'vbb_wpseo_json_ld');
function vbb_wpseo_json_ld()
{
    if(!class_exists('WPSEO_JSON_LD'))
        return;
    global $wp_filter;
    if(!@$wp_filter["wpseo_json_ld"]){
        $vbb_wpseo_json_ld = new WPSEO_JSON_LD();
        $vbb_wpseo_json_ld->json_ld();
    }
}

// add_filter('the_content', 'vbb_add_rating_to_content');
function vbb_add_rating_to_content($content) {
    static $posts_ids_printed_ratings = array();

    if(!function_exists('the_ratings')) return $content;

    if(get_query_var( 'amp' )) return $content;

    global $post;

    if('post' != $post->post_type) return $content;

    if(in_array($post->ID, $posts_ids_printed_ratings ))  return $content;

    $posts_ids_printed_ratings[] = $post->ID;

    $content = the_ratings('div', 0, false) . $content;

    return $content;
}

/**----------------------------Admin Part---------------------------------*/
add_action( 'admin_menu', 'ag_prsd_plugin_menu' );
function ag_prsd_plugin_menu() {
    add_options_page( 
        'Structured Data Postratings',
        'Structured Data PR',
        'manage_options',
        'ag_struct_data_postrating',
        'ag_prsd_admin_options');
}

function ag_prsd_admin_options() {
    //must check that the user has the required capability 
    if (!current_user_can('manage_options'))
    {
        wp_die( __('You do not have sufficient permissions to access this page.') );
    }

    // Read in existing option value from database
    $logo = get_option( '_ag_sd_logo' );

    // See if the user has posted us some information
    // If they did, this hidden field will be set to 'Y'
    if( isset($_POST[ 'ag_sd_submit_hidden' ]) && $_POST[ 'ag_sd_submit_hidden' ] == 'Y' ) {
        // Read their posted value
        $logo = $_POST[ '_ag_sd_logo' ];

        // Save the posted value in the database
        update_option( '_ag_sd_logo', $logo );

        // Put a "settings saved" message on the screen ?>
        <div class="updated"><p><strong><?php _e('settings saved.', 'ag-structured-data-postrating' ); ?></strong></p></div>
        <?php
    }

    // Now display the settings editing screen
    echo '<div class="wrap">';

    // header
    echo "<h2>" . __( 'Structured Data postratings', 'ag-structured-data-postrating' ) . "</h2>";
    ?>

    <form name="form1" method="post" action="">
        <input type="hidden" name="ag_sd_submit_hidden" value="Y">

        <p><?php _e("Logo:", 'ag-structured-data-postrating' ); ?> 
            <input type="text" name="_ag_sd_logo" value="<?php echo $logo; ?>" size="5">
            <button class="set_custom_images button">Set Image ID</button>
        </p><hr />

        <p class="submit">
        <input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
        </p>

        </form>
    </div>
    <script>
        jQuery(document).ready(function() {
            var $ = jQuery;
            if ($('.set_custom_images').length > 0) {
                if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
                    $(document).on('click', '.set_custom_images', function(e) {
                        e.preventDefault();
                        var button = $(this);
                        var id = button.prev();
                        wp.media.editor.send.attachment = function(props, attachment) {
                            id.val(attachment.id);
                        };
                        wp.media.editor.open(button);
                        return false;
                    });
                }
            }
        });
    </script>
<?php
}