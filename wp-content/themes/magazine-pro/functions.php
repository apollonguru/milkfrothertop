<?php
/**
 * Magazine Pro.
 *
 * This file adds the functions to the Magazine Pro Theme.
 *
 * @package Magazine
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://my.studiopress.com/themes/magazine/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'magazine_localization_setup' );
function magazine_localization_setup(){
	load_child_theme_textdomain( 'magazine-pro', get_stylesheet_directory() . '/languages' );
}

// Add the theme helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Add the Customizer options.
include_once( get_stylesheet_directory() . '/lib/customize.php' );

// Add the Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

// Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Add the WooCommerce customizer CSS.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Include notice to install Genesis Connect for WooCommerce.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', __( 'Magazine Pro', 'magazine-pro' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/magazine/' );
define( 'CHILD_THEME_VERSION', '3.2.4' );

// Enqueue required fonts, scripts, and styles.
add_action( 'wp_enqueue_scripts', 'magazine_enqueue_scripts' );
function magazine_enqueue_scripts() {

	wp_enqueue_script( 'magazine-entry-date', get_stylesheet_directory_uri() . '/js/entry-date.min.js', array( 'jquery' ), '1.0.0', true );

	wp_enqueue_style( 'dashicons' );

	// wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,400|Raleway:400,500,900', array(), CHILD_THEME_VERSION );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'magazine-responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menus' . $suffix . '.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'magazine-responsive-menu',
		'genesis_responsive_menu',
		magazine_responsive_menu_settings()
	);

    wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '201812', true );

}

// Define our responsive menu settings.
function magazine_responsive_menu_settings() {

	$settings = array(
		'mainMenu'    => __( 'Menu', 'magazine-pro' ),
		'subMenu'     => __( 'Submenu', 'magazine-pro' ),
		'menuClasses' => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
				'.nav-secondary',
			),
		),
	);

	return $settings;

}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add image sizes.
add_image_size( 'home-middle', 630, 350, true );
add_image_size( 'home-top', 750, 420, true );
add_image_size( 'sidebar-thumbnail', 100, 100, true );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'default-text-color' => '000000',
	'flex-height'        => true,
	'header-selector'    => '.site-title a',
	'header-text'        => false,
	'height'             => 180,
	'width'              => 760,
) );

// Rename menus.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Before Header Menu', 'magazine-pro' ), 'secondary' => __( 'After Header Menu', 'magazine-pro' ) ) );

// Remove skip link for primary navigation.
add_filter( 'genesis_skip_links_output', 'magazine_skip_links_output' );
function magazine_skip_links_output( $links ) {

	if ( isset( $links['genesis-nav-primary'] ) ) {
		unset( $links['genesis-nav-primary'] );
	}

	$new_links = $links;
	array_splice( $new_links, 1 );

	if ( has_nav_menu( 'secondary' ) ) {
		$new_links['genesis-nav-secondary'] = __( 'Skip to secondary menu', 'magazine-pro' );
	}

	return array_merge( $new_links, $links );

}

// Add ID to secondary navigation.
add_filter( 'genesis_attr_nav-secondary', 'magazine_add_nav_secondary_id' );
function magazine_add_nav_secondary_id( $attributes ) {

	$attributes['id'] = 'genesis-nav-secondary';

	return $attributes;

}

// Reposition the primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_before_header', 'genesis_do_nav' );

// Remove output of primary navigation right extras.
remove_filter( 'genesis_nav_items', 'genesis_nav_right', 10, 2 );
remove_filter( 'wp_nav_menu_items', 'genesis_nav_right', 10, 2 );

// Remove navigation meta box.
add_action( 'genesis_theme_settings_metaboxes', 'magazine_remove_genesis_metaboxes' );
function magazine_remove_genesis_metaboxes( $_genesis_theme_settings_pagehook ) {
	remove_meta_box( 'genesis-theme-settings-nav', $_genesis_theme_settings_pagehook, 'main' );
}

// Add primary-nav class if primary navigation is used.
add_filter( 'body_class', 'magazine_no_nav_class' );
function magazine_no_nav_class( $classes ) {

	$menu_locations = get_theme_mod( 'nav_menu_locations' );

	if ( ! empty( $menu_locations['primary'] ) ) {
		$classes[] = 'primary-nav';
	}

	return $classes;

}

// Customize search form input box text.
add_filter( 'genesis_search_text', 'magazine_search_text' );
function magazine_search_text( $text ) {
	return esc_attr( __( 'Search the site ...', 'magazine-pro' ) );
}

// Remove entry meta in entry footer.
add_action( 'genesis_before_entry', 'magazine_remove_entry_meta' );
function magazine_remove_entry_meta() {

	// Remove if not single post.
	if ( ! is_single() ) {
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
	}

}

// Add support for 3-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 3 );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Relocate after entry widget.
remove_action( 'genesis_after_entry', 'genesis_after_entry_widget_area' );
add_action( 'genesis_entry_footer', 'genesis_after_entry_widget_area' );

// Register widget areas.
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'magazine-pro' ),
	'description' => __( 'This is the top section of the homepage.', 'magazine-pro' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home - Middle', 'magazine-pro' ),
	'description' => __( 'This is the middle section of the homepage.', 'magazine-pro' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'magazine-pro' ),
	'description' => __( 'This is the bottom section of the homepage.', 'magazine-pro' ),
) );

/* DG custom */

remove_action( 'wp_head', 'noindex', 1 );
add_action( 'wp_head', 'ag_noindex_nofollow_meta', 1 );
function ag_noindex_nofollow_meta(){
    if ( '0' == get_option('blog_public') )
        echo '<meta name="robots" content="noindex,nofollow">';
}

add_filter( 'genesis_seo_title', function($title){

    if(function_exists('get_field') && $logo = get_field('logo', 'option')){

        $alt = get_field('logo_alt', 'option');
        $title = get_field('logo_title', 'option');

        $alt = $alt ? $alt : $logo['alt'];
        $title = $title ? $title : $logo['title'];

        $title = '<a href="'.get_bloginfo( 'url' ).'"><img src="'.$logo['url'].'" alt="'.$alt.'" title="'.$title.'" class="logo"></a>';
    }

    return $title;

} );

add_action( 'genesis_after_header', function(){
    if(is_front_page() && function_exists('get_field') && $main_page_title = get_field('main_page_title', 'option'))
        echo "<h1 class='front-page-title'>" . $main_page_title . "</h1>";
} );

add_action('wp', 'canonical_request');
function canonical_request()
{
    global $page, $post, $wp_query;

    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

    if($uri_parts[0] != strtolower($uri_parts[0])){
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 );
        exit();
    }
    
    // post, page, attachment, preview
    if ( ! is_singular() or is_preview() )
    {
        return;
    }
    $permalink = get_permalink();
    // We don't have access to the number of sub pages here.
    // So we have to hack.
    $max_pages = substr_count(
        $post->post_content, '<!--nextpage-->') + 1;
    if ( 1 < $page and $page <= $max_pages )
    {
        /*
         * Handle different permalink settings, eg:
         * /%year%/%postname%.html or
         * /%year%/%postname%/
         */
        $rev_perma_struct = strrev(get_option('permalink_structure'));
        if ( '/' != $rev_perma_struct[0] )
        {
            $permalink .= "/$page";
        }
        else
        {
            $permalink .= "$page/";
        }
    }
    $host_uri       = 'http'
                    . ( empty ( $_SERVER['HTTPS'] ) ? '' : 's' )
                    . '://' . $_SERVER['HTTP_HOST'];
    $canonical_path = str_replace($host_uri, '', $permalink);
    if ( ! empty ( $_GET ) )
    {
        global $wp;
        // Array
        $allowed = $wp->public_query_vars;
        array_push($allowed, 'replytocom', 'fbclid');
        $out_arr = array();
        foreach ( $_GET as $k => $v )
        {
            if ( in_array($k, $allowed ) )
            {
                $out_arr[] = $k . ( empty ( $v ) ? '' : "=$v" );
            }
        }
        if ( ! empty ( $out_arr ) )
        {
            $canonical_path .= '?' . implode('&', $out_arr);
        }
    }
    if ( $canonical_path == $_SERVER['REQUEST_URI'] )
    {
        return;
    }
    // Debug current result:
    #print '<pre>' . var_export($canonical_path, TRUE) . '</pre>';
    // Change it or return 'false' to stop the redirect.
    $canonical_path = apply_filters(
        'toscho_canonical_path',
        $canonical_path
    );
    if ( FALSE != $canonical_path )
    {
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 );
        exit();
    }
    return;
}

add_filter( 'genesis_register_sidebar_defaults', 'apollon_genesis_a11y_register_sidebar_defaults', 11 );
function apollon_genesis_a11y_register_sidebar_defaults( $args ) {

    $args['before_title'] = '<p class="widgettitle widget-title">';
    $args['after_title']  = "</p>\n";

    return $args;
}

add_filter( 'genesis_sidebar_title_output', '__return_empty_string' );

add_action('template_redirect', 'cyb_add_last_modified_header');
function cyb_add_last_modified_header($headers) {

	header("Cache-Control: public, max-age=604800");

	$date = strtotime("+7 day");

	header("Expires: " . date('D, d M Y H:i:s', $date) . "");

    //Check if we are in a single post of any type (archive pages has not modified date)
    if( is_singular() ) {
        $post_id = get_queried_object_id();
        if( $post_id ) {
            header("Last-Modified: " . get_the_modified_time("D, d M Y H:i:s", $post_id) );
        }
    }

}

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

add_filter('after_setup_theme', 'remove_redundant_shortlink');
function remove_redundant_shortlink() {
    // remove HTML meta tag
    // <link rel='shortlink' href='http://example.com/?p=25' />
    remove_action('wp_head', 'wp_shortlink_wp_head', 10);

    // remove HTTP header
    // Link: <https://example.com/?p=25>; rel=shortlink
    remove_action( 'template_redirect', 'wp_shortlink_header', 11);
}

/**
 * Remove feed links from wp_head
 */
add_action( 'wp_head', 'apollon_remove_feed_links', 1 );
function apollon_remove_feed_links()
{
    remove_action( 'wp_head', 'feed_links', 2 );
    remove_action( 'wp_head', 'feed_links_extra', 3 );
}

add_filter( 'genesis_attr_entry', 'remove_genesis_structured_data');

add_filter( 'genesis_attr_author-box', 'remove_genesis_structured_data');

add_filter( 'genesis_attr_entry-author', 'remove_genesis_structured_data');

function remove_genesis_structured_data($attr)
{
	if(isset($attr['itemscope'])){
        unset( $attr['itemscope']);
        unset( $attr['itemtype']);
    }
    return $attr;
}

add_filter( 'genesis_breadcrumb_args', function($args){
    $args['labels']['prefix'] = '';
    return $args;
} );

add_filter( 'genesis_breadcrumb_link', function($link){
    static $iterator;
    if(!$iterator) $iterator = 0;
    $iterator++;
    $link .= '<meta itemprop="position" content="'.$iterator.'">';
    return $link;
} );

add_filter( "genesis_markup_entry-title_open", function($open, $args){
    if( isset($args['params']['is_widget']) && $args['params']['is_widget'])
        $open = '<p class="entry-title" itemprop=" headline">';
    return $open;
}, 10, 2 );

add_filter( "genesis_markup_entry-title_close", function($close, $args){
    if(isset($args['params']['is_widget']) && $args['params']['is_widget'])
        $close = '</p>';
    return $close;
}, 10, 2 );

add_filter( 'comment_form_defaults', function($args){
    $args['title_reply_before'] = '<p id="reply-title" class="comment-reply-title">';
    $args['title_reply_after'] = '</p>';
    return $args;
} );

add_action( 'wp', function(){
    if(is_singular( 'post' )){
        add_filter( 'genesis_attr_body', 'remove_genesis_structured_data' );
    }
} );

function custom_rating_image_extension() {
    return 'png';
}
add_filter( 'wp_postratings_image_extension', 'custom_rating_image_extension' );

add_filter( 'genesis_get_seo_meta_description', function($description){
    if(is_search()){
        if(function_exists('get_field') && $search_description = get_field('search_description', 'option')){
            $description = $search_description;
        }
    }
    return $description;
} );

add_filter( 'genesis_get_seo_meta_keywords', function($keywords){
    if(is_search() && function_exists('get_field') && $search_keywords = get_field('search_keywords', 'option')){
        $keywords = $search_keywords;
    }
    return $keywords;
} );

add_filter( 'genesis_search_title_output', function($title){
    $search_query = get_search_query();
    if('' == $search_query){
        $title = str_replace(' Results for:', '', $title);
    }
    return $title;
} );

add_filter( 'document_title_parts', function($parts){
    if(is_search() && function_exists('get_field') && $search_title = get_field('search_title', 'option')){
        $parts['title'] = $search_title;
    }
    return $parts;
});

add_action('wp_enqueue_scripts', 'ag_scripts_to_footer', 1000);
function ag_scripts_to_footer()
{

    /*if (!is_admin()) {
        wp_dequeue_script('jquery-migrate');
        wp_enqueue_script('jquery-migrate', false, array(), false, true);
    }*/

    wp_dequeue_script('svg-x-use');
    wp_enqueue_script('svg-x-use-min', get_stylesheet_directory_uri() . '/js/svgxuse.min.js', array(), '1.1.21', true );

    // wp_dequeue_script('genesis-simple-share-waypoint-js');
    // wp_deregister_script( 'genesis-simple-share-waypoint-js' );

    wp_dequeue_style('contact-form-7');
    wp_dequeue_style('wp-postratings');
    wp_dequeue_style('ez-icomoon');
    wp_dequeue_style('ez-toc');
    wp_dequeue_style('simple-social-icons-font');
    wp_dequeue_style('tablepress-responsive-tables');
    // wp_dequeue_style('tablepress-default');
    wp_dequeue_style('dashicons');
    add_filter( 'tablepress_responsive_tables_enqueue_flip_css_file', '__return_false' );
}

 function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];

        if ( $script->deps ) { // Check whether the script has any dependencies
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
 }
 
 add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

add_filter( 'script_loader_tag', 'ag_defer_scripts', 10, 3 );
function ag_defer_scripts( $tag, $handle, $src ) {

    // The handles of the enqueued scripts we want to defer
    $defer_scripts = array( 
        'contact-form-7',
        'jquery-migrate',
        'svg-x-use',
        'magazine-entry-date',
        'genesis-simple-share-plugin-js'
    );

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }
    
    return $tag;
}

function prefix_add_footer_styles() {
    $tdu_pl = get_template_directory_uri() . '/../../plugins/';
    wp_enqueue_style( 'contact-form-7', $tdu_pl . 'contact-form-7/includes/css/styles.css' );

    wp_enqueue_style( 'wp-postratings', $tdu_pl . 'wp-postratings/css/postratings-css.css' );
    wp_enqueue_style( 'contact-form-7', $tdu_pl . 'contact-form-7/includes/css/styles.css' );
    wp_enqueue_style( 'ez-icomoon', $tdu_pl . 'easy-table-of-contents/vendor/icomoon/style.min.css' );
    wp_enqueue_style( 'contactez-toc', $tdu_pl . 'easy-table-of-contents/assets/css/screen.min.css' );
    wp_enqueue_style( 'simple-social-icons-font', $tdu_pl . 'simple-social-icons/css/style.css' );
    // wp_enqueue_style( 'tablepress-default-min', $tdu_pl . 'tablepress/css/default.min.css' );
    wp_enqueue_style( 'dashicons', $tdu_pl . get_include_path() . '/css/dashicons.min.css' );
    wp_enqueue_style( 'tablepress-responsive-tables', $tdu_pl . 'tablepress-responsive-tables/css/responsive.dataTables.min.css' );

    // wp_register_script( 'genesis-simple-share-waypoint-js', $tdu_pl . 'genesis-simple-share/lib/assets/js/waypoints.min.js', array( 'jquery' ), '0.1.0');

    wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto:300,400|Raleway:400,500,900', array(), CHILD_THEME_VERSION );
};
add_action( 'get_footer', 'prefix_add_footer_styles' );

add_filter( 'genesis_get_seo_meta_description', function($description){
    if (is_paged()) {
        $paged = get_query_var( 'paged' );
        $description = 'Page ' . $paged . ' | ' . $description;
    }
    return $description;
} );

add_filter( 'genesis_get_seo_meta_keywords', function($keywords){
    if (is_paged()) {
        $paged = get_query_var( 'paged' );
        $keywords = 'page ' . $paged . ', ' . $keywords;
    }
    return $keywords;
} );

add_filter( 'genesis_canonical_url', 'fix_canonical_paged_page' );
function fix_canonical_paged_page( $canonical ){

    if(is_paged()){

        global $wp_query;

        if ( is_category() || is_tag() || is_tax() ){

            if ( ! $id = $wp_query->get_queried_object_id() ) {
                return null;
            }

            $taxonomy = $wp_query->queried_object->taxonomy;

            $canonical = get_term_link( (int) $id, $taxonomy );
        }

        if ( is_author() ) {

            if ( ! $id = $wp_query->get_queried_object_id() ) {
                return null;
            }

            $canonical = get_author_posts_url( $id );

        }

    }

    return $canonical;
}

add_action( 'genesis_archive_title_descriptions', function ()
{
    if(is_paged())
        remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_intro_text', 12);

    return;
} );

// взято из функции genesis_get_sitemap()
function ag_get_sitemap( $atts ) {
    
    $default = array(
        'heading' => 'h2',
        'exclude' => ''
    );

    $atts = shortcode_atts( $default, $atts );

    $exclude = '';

    if($atts['exclude'])
        $exclude = '&exclude='.$atts['exclude'];

    $post_counts = wp_count_posts();
    if ( $post_counts->publish > 0 ) {
        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', __( 'Recent Posts:', 'genesis' ), $default['heading'] );
        $sitemap .= sprintf( '<ul>%s</ul>', wp_get_archives( 'type=postbypost&limit=100&echo=0' ) );

        $sitemap .= sprintf( '<%2$s>%1$s</%2$s>', __( 'Categories:', 'genesis' ), $default['heading'] );
        $sitemap .= sprintf( '<ul>%s</ul>', wp_list_categories( 'sort_column=name&title_li=&echo=0' ) );
    }

    $sitemap = wp_kses_post( $sitemap );

    return $sitemap;

}

add_shortcode( 'apollon-html-sitemap', 'ag_get_sitemap' );

add_filter( 'the_content', function($content){
    if( preg_match_all( '/(<h([1-6]{1})[^>]*>).*<\/h\2>/msuU', $content, $matches, PREG_SET_ORDER ) ){

        $headings = array(3);

        if ( count( $headings ) != 6 ) {

            $new_matches = array();

            for ( $i = 0; $i < count( $matches ); $i ++ ) {

                if ( in_array( $matches[ $i ][2], $headings ) ) {

                    $new_matches[] = $matches[ $i ];
                }
            }
            $matches = $new_matches;
        }

        for ( $i = 0; $i < count( $matches ); $i++ ) {

            // get anchor and add to find and replace arrays
            $anchor    =  ad_url_anchor_target( $matches[ $i ][0] );
            $find[]    = $matches[ $i ][0];
            $replace[] = str_replace(
                array(
                    $matches[ $i ][1],                // start of heading
                    '</h' . $matches[ $i ][2] . '>'   // end of heading
                ),
                array(
                    $matches[ $i ][1] . '<span class="ez-toc-section" id="' . $anchor . '">',
                    '</span></h' . $matches[ $i ][2] . '>'
                ),
                $matches[ $i ][0]
            );
        }

        $content = ag_mb_find_replace($find, $replace, $content);
    }

    return $content;
} );

function ad_url_anchor_target($title){
    $return = FALSE;

    if ( $title ) {

        // WP entity encodes the post content.
        $return = html_entity_decode( $title, ENT_QUOTES, get_option( 'blog_charset' ) );

        $return = trim( strip_tags( $return ) );

        // Convert accented characters to ASCII.
        $return = remove_accents( $return );

        // replace newlines with spaces (eg when headings are split over multiple lines)
        $return = str_replace( array( "\r", "\n", "\n\r", "\r\n" ), ' ', $return );

        // Remove `&amp;` and `&nbsp;` NOTE: in order to strip "hidden" `&nbsp;`,
        // title needs to be converted to HTML entities.
        // @link https://stackoverflow.com/a/21801444/5351316
        $return = htmlentities2( $return );
        $return = str_replace( array( '&amp;', '&nbsp;' ), ' ', $return );
        $return = html_entity_decode( $return, ENT_QUOTES, get_option( 'blog_charset' ) );

        // remove non alphanumeric chars
        $return = preg_replace( '/[^a-zA-Z0-9 \-_]*/', '', $return );

        // convert spaces to _
        $return = preg_replace( '/\s+/', '_', $return );

        // remove trailing - and _
        $return = rtrim( $return, '-_' );

        // lowercase everything?
        $return = strtolower( $return );

        $return = str_replace( '_', '-', $return );
        $return = str_replace( '--', '-', $return );
    }

    return $return;
}

function ag_mb_find_replace( $find = FALSE, $replace = FALSE, $string = '' ) {

    if ( is_array( $find ) && is_array( $replace ) && $string ) {

        // check if multibyte strings are supported
        if ( function_exists( 'mb_strpos' ) ) {

            for ( $i = 0; $i < count( $find ); $i ++ ) {

                $string = mb_substr(
                              $string,
                              0,
                              mb_strpos( $string, $find[ $i ] )
                          ) .    // everything before $find
                          $replace[ $i ] . // its replacement
                          mb_substr(
                              $string,
                              mb_strpos( $string, $find[ $i ] ) + mb_strlen( $find[ $i ] )
                          )    // everything after $find
                ;
            }

        } else {

            for ( $i = 0; $i < count( $find ); $i ++ ) {

                $string = substr_replace(
                    $string,
                    $replace[ $i ],
                    strpos( $string, $find[ $i ] ),
                    strlen( $find[ $i ] )
                );
            }
        }
    }

    return $string;
}

add_action( 'genesis_before', function(){
    if(is_front_page() || is_singular( 'post' )) { ?>
    <div id="progress-container">
        <div class="progress-bar" id="progress-bar"></div>
    </div>
    <?php
    }
}, 20 );

add_action( 'genesis_before', function(){
    wp_enqueue_style( 'font-awesome' ); ?>
    <a id="button"></a>
<?php
} );

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page('Custom Site Options');
    
}

add_filter( 'shortcode_atts_button', function($out, $pairs, $atts, $shortcode){
    if(!isset($atts['target'])){
        $out['target'] = "blank";
    }

    if(!isset($atts['rel'])){
        $out['rel'] = "nofollow";
    }

    return $out;
}, 10, 4 );

remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

add_action( 'genesis_entry_header', 'ag_genesis_do_breadcrumbs' );

class AG_Genesis_Breadcrumb extends Genesis_Breadcrumb
{
    protected function get_post_crumb()
    {
        $categories = get_the_category();

        $cat_crumb = '';

        if ( 1 === count( $categories ) ) {
            // If in single category, show it, and any parent categories.
            $cat_crumb = $this->get_term_parents( $categories[0]->cat_ID, 'category', true ) /*. $this->args['sep']*/;
        }
        if ( count( $categories ) > 1 ) {
            if ( $this->args['heirarchial_categories'] ) {
                // Show parent categories - see if one is marked as primary and try to use that.
                $primary_category_id = get_post_meta( get_the_ID(), '_category_permalink', true ); // Support for sCategory Permalink plugin.
                if ( ! $primary_category_id && function_exists( 'yoast_get_primary_term_id' ) ) {
                    // Support for Yoast SEO plugin, even if the Yoast Breadcrumb feature is not enabled.
                    $primary_category_id = yoast_get_primary_term_id();
                }
                if ( $primary_category_id ) {
                    $cat_crumb = $this->get_term_parents( $primary_category_id, 'category', true ) . $this->args['sep'];
                } else {
                    $cat_crumb = $this->get_term_parents( $categories[0]->cat_ID, 'category', true ) . $this->args['sep'];
                }
            } else {
                $crumbs = array();
                // Don't show parent categories (unless the post happen to be explicitly in them).
                foreach ( $categories as $category ) {
                    $crumbs[] = $this->get_breadcrumb_link(
                        get_category_link( $category->term_id ),
                        '',
                        $category->name
                    );
                }
                $cat_crumb = implode( $this->args['list_sep'], $crumbs ) . $this->args['sep'];
            }
        }

        $crumb = $cat_crumb/* . single_post_title( '', false )*/;

        return apply_filters( 'genesis_post_crumb', $crumb, $this->args, $cat_crumb );
    }

    protected function get_term_parents( $parent_id, $taxonomy, $link = false, array $visited = array() ) {

        $parent = get_term( (int) $parent_id, $taxonomy );

        if ( is_wp_error( $parent ) ) {
            return '';
        }

        if ( $parent->parent && ( $parent->parent != $parent->term_id ) && ! in_array( $parent->parent, $visited ) ) {
            $visited[] = $parent->parent;
            $chain[]   = $this->get_term_parents( $parent->parent, $taxonomy, true, $visited );
        }

        if ( $link && ! is_wp_error( get_term_link( get_term( $parent->term_id, $taxonomy ), $taxonomy ) ) ) {

            $parent_name = $parent->name;

            if(function_exists('get_field') && $icon = get_field('icon', 'category_'.$parent->term_id)){

                $parent_name = $icon . ' ' . $parent_name;
            }

            $chain[] = $this->get_breadcrumb_link(
                get_term_link( get_term( $parent->term_id, $taxonomy ), $taxonomy ),
                '',
                $parent_name
            );
        } else {
            $chain[] = $parent->name;
        }

        return implode( $this->args['sep'], $chain );

    }
}

function ag_genesis_breadcrumb( $args = array() ) {

    global $_ag_genesis_breadcrumb;

    if ( ! $_ag_genesis_breadcrumb ) {
        $_ag_genesis_breadcrumb = new AG_Genesis_Breadcrumb();
    }

    $_ag_genesis_breadcrumb->output( $args );

}

function ag_genesis_do_breadcrumbs() {

    if (
        ( is_single() && ! genesis_get_option( 'breadcrumb_single' ) ) ||
        ( is_page() && ! genesis_get_option( 'breadcrumb_page' ) ) ||
        ( is_404() && ! genesis_get_option( 'breadcrumb_404' ) ) ||
        ( is_attachment() && ! genesis_get_option( 'breadcrumb_attachment' ) ) ||
        ( ( 'posts' === get_option( 'show_on_front' ) && is_home() ) && ! genesis_get_option( 'breadcrumb_home' ) ) ||
        ( ( 'page' === get_option( 'show_on_front' ) && is_front_page() ) && ! genesis_get_option( 'breadcrumb_front_page' ) ) ||
        ( ( 'page' === get_option( 'show_on_front' ) && is_home() ) && ! genesis_get_option( 'breadcrumb_posts_page' ) ) ||
        ( ( is_archive() || is_search() ) && ! genesis_get_option( 'breadcrumb_archive' ) )
    ) {
        return;
    }

    $breadcrumb_markup_open = sprintf( '<div %s>', genesis_attr( 'breadcrumb' ) );

    if ( function_exists( 'bcn_display' ) ) {
        echo $breadcrumb_markup_open;
        bcn_display();
        echo '</div>';
    } elseif ( function_exists( 'breadcrumbs' ) ) {
        breadcrumbs();
    } elseif ( function_exists( 'crumbs' ) ) {
        crumbs();
    } elseif ( class_exists( 'WPSEO_Breadcrumbs' ) && genesis_get_option( 'breadcrumbs-enable', 'wpseo_titles' ) ) {
        yoast_breadcrumb( $breadcrumb_markup_open, '</div>' );
    } elseif ( function_exists( 'yoast_breadcrumb' ) && ! class_exists( 'WPSEO_Breadcrumbs' ) ) {
        yoast_breadcrumb( $breadcrumb_markup_open, '</div>' );
    } else {
        ag_genesis_breadcrumb();
    }
}

add_action( 'genesis_entry_header', function(){
    if((is_singular('post') || is_archive()) && function_exists('ratings_shortcode'))
        echo ratings_shortcode(array());
} );

if(function_exists('wp_rp_add_related_posts_hook')){
    remove_filter('the_content', 'wp_rp_add_related_posts_hook', 10);
    add_filter('the_content', 'ag_wp_rp_add_related_posts_hook', 20);
}

function ag_wp_rp_add_related_posts_hook($content) {
    $related_posts_html = wp_rp_add_related_posts_hook('not_empty_string_for_remove');

    $related_posts_html = str_replace('not_empty_string_for_remove', '', $related_posts_html);

    $related_posts_html = heading_hn_replace($related_posts_html);

    return $content . $related_posts_html;
}

function heading_hn_replace($content){
    $tag_to_replace = 'div';
    
    $reg_open_tag = '/<(h\d+)/i';
    
    $reg_close_tag = '/<\/(h\d+)>/i';

    $add_to_existing_css_class_pattern = '/<h(\d+) [^>]*class=([\"\'])(.*?)\2[^>]*/i';

    $add_to_existing_css_class_replacement = '<h$1 class=$2$3 h$1_replaced$2';

    $content = preg_replace($add_to_existing_css_class_pattern, $add_to_existing_css_class_replacement, $content);

    $add_css_class_pattern = '/<(h\d+)>/i';

    $add_css_class_replacement = '<$1 class="$1_replaced">';

    $content = preg_replace($add_css_class_pattern, $add_css_class_replacement, $content);

    $content = preg_replace($reg_open_tag, '<'.$tag_to_replace, $content);
    
    $content = preg_replace($reg_close_tag, '</'.$tag_to_replace.'>', $content);

    return $content;
}

add_filter( 'walker_nav_menu_start_el', function($item_output, $item, $depth, $args){

    if(in_array('current-menu-item', $item->classes)){
        $item_output = '<span class="a_replaced"><span itemprop="name">'.$item->title.'</span></span>';
    }

    return $item_output;
},10 , 4 );

add_filter( 'genesis_comment_list_args', function($args){
    $args['callback'] = 'ag_genesis_html5_comment_callback';
    return $args;
} );

// see function genesis_html5_comment_callback
function ag_genesis_html5_comment_callback( $comment, array $args, $depth ) {

    $GLOBALS['comment'] = $comment; ?>

    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
    <article <?php echo genesis_attr( 'comment' ); ?>>

        <?php
        /**
         * Fires inside single comment callback, before comment markup.
         *
         * @since 1.1.0
         */
        do_action( 'genesis_before_comment' );
        ?>

        <header <?php echo genesis_attr( 'comment-header' ); ?>>
            <p <?php echo genesis_attr( 'comment-author' ); ?>>
                <?php
                echo get_avatar( $comment, $args['avatar_size'] );

                $author = get_comment_author();
                $url    = get_comment_author_url();

                if ( ! empty( $url ) && 'http://' !== $url ) {
                    $author = sprintf( '<a href="%s" %s>%s</a>', esc_url( $url ), genesis_attr( 'comment-author-link' ), $author );
                }

                /**
                 * Filter the "comment author says" text.
                 *
                 * Allows developer to filter the "comment author says" text so it can say something different, or nothing at all.
                 *
                 * @since unknown
                 *
                 * @param string $text Comment author says text.
                 */
                $comment_author_says_text = apply_filters( 'comment_author_says_text', __( 'says', 'genesis' ) );

                if ( ! empty( $comment_author_says_text ) ) {
                    $comment_author_says_text = '<span class="says">' . $comment_author_says_text . '</span>';
                }

                printf( '<span itemprop="name">%s</span> %s', $author, $comment_author_says_text );
                ?>
            </p>

            <?php
            /**
             * Allows developer to control whether to print the comment date.
             *
             * @since 2.2.0
             *
             * @param bool   $comment_date Whether to print the comment date.
             * @param string $post_type    The current post type.
             */
            $comment_date = apply_filters( 'genesis_show_comment_date', true, get_post_type() );

            if ( $comment_date ) {
                printf( '<p %s>', genesis_attr( 'comment-meta' ) );
                printf( '<time %s>', genesis_attr( 'comment-time' ) );
                printf( '<a href="%s" %s>', esc_url( get_comment_link( $comment->comment_ID ) ), genesis_attr( 'comment-time-link' ) );
                echo    esc_html( get_comment_date() );
                echo    '</a></time></p>';
            }

            edit_comment_link( __( '(Edit)', 'genesis' ), ' ' );
            ?>
        </header>

        <div <?php echo genesis_attr( 'comment-content' ); ?>>
            <?php if ( ! $comment->comment_approved ) : ?>
                <?php
                /**
                 * Filter the "comment awaiting moderation" text.
                 *
                 * Allows developer to filter the "comment awaiting moderation" text so it can say something different, or nothing at all.
                 *
                 * @since unknown
                 *
                 * @param string $text Comment awaiting moderation text.
                 */
                $comment_awaiting_moderation_text = apply_filters( 'genesis_comment_awaiting_moderation', __( 'Your comment is awaiting moderation.', 'genesis' ) );
                ?>
                <p class="alert"><?php echo $comment_awaiting_moderation_text; ?></p>
            <?php endif; ?>

            <?php comment_text(); ?>
        </div>

        <?php
        comment_reply_link( array_merge( $args, array(
            'depth'  => $depth,
            'before' => sprintf( '<div %s>', genesis_attr( 'comment-reply' ) ),
            'after'  => '</div>',
        ) ) );
        ?>

        <?php
        /**
         * Fires inside legacy single comment callback, after comment markup.
         *
         * @since 1.1.0
         */
        do_action( 'genesis_after_comment' );
        ?>

    </article>
    <?php
    // No ending </li> tag because of comment threading.
}

add_filter( 'comment_reply_link', 'remove_href_from_reply_link' );

function remove_href_from_reply_link($link){

    preg_match('/<a [^>]*href=([\"\']).+(\?replytocom=\d+).+?\1/i', $link, $result);

    if( 3 == count($result) ){

        $link = str_replace($result[2], '', $link);

    }

    return $link;
}

function webp_upload_mimes( $existing_mimes ) {
    // add webp to the list of mime types
    $existing_mimes['webp'] = 'image/webp';

    // return the array back to the function with our added mime type
    return $existing_mimes;
}
add_filter( 'mime_types', 'webp_upload_mimes' );

function change_mce_options( $init ) {
    $opts = '*[*]';
    $init['valid_elements'] = $opts;
    $init['extended_valid_elements'] = $opts;
    return $init;
}
add_filter('tiny_mce_before_init', 'change_mce_options');

add_filter('the_content', 'ag_add_amazon_ad_to_post', 25);
function ag_add_amazon_ad_to_post($content)
{
    if (is_singular( 'post' )) {
        
        $hide = function_exists('get_field') ? get_field('hide_amzn_related_prod') : false;

        if(!$hide){

            $ad_id = 'c7df4fb2-2150-4eb1-b9d4-44203ce5e5ec';
            
            $amzn_ad  = '<div id="amzn-assoc-ad-'.$ad_id.'"></div>';
            $amzn_ad .= '<script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId='.$ad_id.'"></script>';

            $content = $content . $amzn_ad;   
        }
        
    }
    return $content;
}


add_filter('the_content', function( $content ){
    if((is_single() && is_singular()) || (is_singular() && is_front_page())){
      $content = '<div class="disclosure" data-nosnippet><p><span>Disclosure:</span> As an Amazon Associate I earn from qualifying purchases. There may be affiliate links in this post. If you buy products through these affiliate links, I may earn some commission directly from Amazon Company. Thanks for helping my site grow!</p></div>' . $content;
    }
    return $content;
  }, 5);

require get_theme_file_path('/includes/hooks/actions.php');

require get_theme_file_path('/includes/hooks/filters.php');

require get_theme_file_path('/includes/hooks/hook-functions/action-functions.php');

require get_theme_file_path('/includes/hooks/hook-functions/filter-functions.php');

/* END DG custom */