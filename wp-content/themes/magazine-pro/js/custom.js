jQuery(document).ready(function($){

	toTopButton.init();

	progressBar.init();

	mobileHeaderElements();
});

var toTopButton = {
	init: function(){
		toTopButton.config = {
			btn: jQuery( "#button" ),
			body: jQuery('html, body')
		};

		toTopButton.setup();
	},

	setup: function(){
		jQuery(window).scroll(function() {
			if (jQuery(window).scrollTop() > 300) {
				toTopButton.config.btn.addClass('show');
			} else {
				toTopButton.config.btn.removeClass('show');
			}
		});

		toTopButton.config.btn.on('click', function(e) {
			e.preventDefault();
			toTopButton.config.body.animate({scrollTop:0}, '300');
		});
	}

}

var progressBar = {
	init: function(){
		var mainEl = document.getElementsByTagName('main')[0];

		if(!mainEl) return;

		var article = mainEl.getElementsByTagName('article')[0];

		if(!article) return;
		
		progressBar.config = {
			article:					article,
			scrollIndicatorEl:			document.getElementById("progress-bar"),
			scrollIndicatorContainer:	document.getElementById("progress-container"),
			winScroll:					null
		};

		if(!progressBar.config.scrollIndicatorContainer) return;

		window.onscroll = progressBar.setup;

	},

	setup: function() {
		var newScroll = document.body.scrollTop || document.documentElement.scrollTop,
			scrolled;
		
		if(progressBar.config.winScroll == newScroll)
			return;
		
		progressBar.config.winScroll = newScroll;

		var height = progressBar.config.article.clientHeight - document.documentElement.clientHeight;

		scrolled = ((progressBar.config.winScroll - progressBar.config.article.offsetTop) / height) * 100;

		if(newScroll < progressBar.config.article.offsetTop){
			scrolled = 0;
		}

		if( scrolled > 100)
			scrolled = 100;
		else if(scrolled < 0)
			scrolled = 0;

		if(scrolled > 0)
			progressBar.config.scrollIndicatorContainer.classList.add('active');
		else
			progressBar.config.scrollIndicatorContainer.classList.remove('active');

		progressBar.config.scrollIndicatorEl.style.width = scrolled + "%";
	}
}

function mobileHeaderElements(){
	var $ = jQuery;

	var $header = $('header.site-header'),
		$searchForm = $header.find('form.search-form');
		$menuButton = $('#genesis-mobile-nav-secondary');

	if($searchForm.length && $menuButton.length){
		
		var $mobileHeaderWrap = $('<div />',{id: 'mobile-header-wrap', class: 'widget_search'}),
			$logo = $header.find('.title-area a').clone(true).addClass('mobile-logo'),
			$mobileSearchFormButton = $( '<a />',{id: 'mobile-search-form-btn', href: '#', class: 'dashicons dashicons-search'} ),
			$mobileSearchForm = $searchForm.clone(true),
			// $mobileNav = $('#genesis-nav-secondary').clone(true),
			$body = $('body'),
			$mobileInputSearch = $mobileSearchForm.find('input[type="search"]');

		$mobileSearchForm.find('meta').remove();
		$mobileSearchForm.stripMicrodata();

		$menuButton.text('');

		$mobileHeaderWrap.append( $logo, $mobileSearchFormButton, $mobileSearchForm, $menuButton );

		$header.after( $mobileHeaderWrap );

		$body.click(function(event) {
			if( !$mobileSearchForm.hasClass('show') ) return;
			var target = $( event.target );
			if ( target.is( $mobileSearchFormButton ) ) return;
			if ( target.is( $mobileInputSearch ) ) return;
			
			$mobileSearchForm.removeClass('show');
		});
		
		$mobileSearchFormButton.click(function(event) {
			event.preventDefault();
			$mobileSearchForm.addClass('show');
		});
	}
}

(function($){
	jQuery.fn.stripMicrodata = function(){
		let attrToRem = ['itemscope', 'itemtype', 'itemprop'];
		let make = function(){
			attrToRem.forEach((attr) => {
				$(this).removeAttr(attr);
				$(this).find('['+attr+']').removeAttr(attr);
			});
		};
		return this.each(make);
	};
})(jQuery);