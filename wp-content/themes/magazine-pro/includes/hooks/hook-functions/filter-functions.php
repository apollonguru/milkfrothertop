<?php

function ag_structured_data_publisher($postratings_structured_data){

	if (!function_exists('get_field')) return $postratings_structured_data;

	$author_id = get_the_author_meta('ID');

	$autor_data = get_field('structured_data', 'user_' . $author_id);

	if( $author_data_array = json_decode( $autor_data ) ) {
		
		$postratings_structured_data['author'] = $author_data_array;
	
	}

	$organization_structured_data = get_field('organization_structured_data', 'option');

	$organization_array = json_decode( $organization_structured_data, true );

	if( $organization_array && array_key_exists('@id', $organization_array) ) {
		
		$postratings_structured_data['publisher'] = array( '@id' => $organization_array['@id']);
	
	}

	return $postratings_structured_data;
}

function ag_replace_wpheader_to_website($attr){

	$attr['itemtype'] = 'https://schema.org/WebSite';

	return $attr;

}

?>