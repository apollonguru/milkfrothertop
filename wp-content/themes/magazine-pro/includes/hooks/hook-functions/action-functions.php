<?php

function ag_structured_data(){

	if( !function_exists('get_field') ) return;

	$organization_structured_data = get_field('organization_structured_data', 'option');

	if( $organization_structured_data = get_field('organization_structured_data', 'option') ) {
	
		echo '<script type="application/ld+json" class="wp-postratings-structured-data">' . $organization_structured_data . '</script>';
	
	}

	if( is_author() ){

		$author_id = get_the_author_meta('ID');

		$structured_data = get_field('structured_data', 'user_' . $author_id);

		if(!($structured_data)) return;

		echo '<script type="application/ld+json" class="wp-postratings-structured-data">' . $structured_data . '</script>';
		
	}
}

function ag_add_itemprop_url_for_website(){

	echo '<meta itemprop="url" content="' . get_site_url() . '"/>';

}

?>